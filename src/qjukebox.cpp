#include "qjukebox.h"
#include "ui_qjukebox.h"
#include <QDirModel>
#include <QTreeView>
#include <QFileSystemModel>
#include <QFileInfo>
#include <QTextStream>
#include <QAction>
#include <QtDebug>
#include <QFileInfo>
#include <QtAwesome/QtAwesome.h>
#include <QMediaPlayer>
#include <QMediaPlaylist>

// todo: clean up source code and create classes and objects

static QFile playlistfile(QDir::homePath() + "/.config/QJukeBox/playlist");

QJukeBox::QJukeBox(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::QJukeBox) {
    ui->setupUi(this);

        qApp->installEventFilter(this);

        awesome = new QtAwesome(qApp);
        awesome->initFontAwesome();

        FileExplorerModel->setRootPath(QDir::homePath());
        QStringList filter;
        filter << "*.mp3" << "*.aac" << "*.wav" << "*.flac" << "*.ogg";
        FileExplorerModel->setNameFilters(filter);

        // Did not pass the filter file disable or hidden, true disable false
        FileExplorerModel->setNameFilterDisables(false);

        QAction *addToPlaylist = new QAction("add to playlist",ui->file_browser);
        QAction *directPlay = new QAction("direct play",ui->file_browser);
        ui->file_browser->addAction(addToPlaylist);
        ui->file_browser->addAction(directPlay);
        ui->file_browser->setContextMenuPolicy(Qt::ActionsContextMenu);
        ui->file_browser->setAnimated(false);
        ui->file_browser->setSortingEnabled(true);
        ui->file_browser->setModel(FileExplorerModel);
        ui->file_browser->resizeColumnToContents(0);

        QAction *removeFromPlaylist = new QAction("remove from playlist",ui->playlist_browser);
        ui->playlist_browser->addAction(removeFromPlaylist);
        ui->playlist_browser->setContextMenuPolicy(Qt::ActionsContextMenu);

        QAction *playItem = new QAction("play",ui->playlist_browser);
        ui->playlist_browser->addAction(playItem);

        ui->play_pause_btn->setIcon(awesome->icon(fa::play));
        ui->stop_btn->setIcon(awesome->icon(fa::stop));
        ui->seek_left_btn->setIcon(awesome->icon(fa::backward));
        ui->seek_right_btn->setIcon(awesome->icon(fa::forward));
        ui->settings_btn->setIcon(awesome->icon(fa::cog));
        ui->exit_btn->setIcon(awesome->icon(fa::windowclose));

        connect(addToPlaylist,SIGNAL(triggered(bool)),SLOT(addToPlaylist()));
        connect(directPlay,SIGNAL(triggered(bool)),SLOT(directPlay()));
        connect(removeFromPlaylist,SIGNAL(triggered(bool)),SLOT(removeFromPlaylist()));
        connect(playItem,SIGNAL(triggered(bool)),SLOT(playItem()));

        player = new QMediaPlayer;
        player_playlist = new QMediaPlaylist(player);

        ui->progressBar->setRange(0,100);
        connect(player, SIGNAL(positionChanged(qint64)),this, SLOT(updateProgress(qint64)));
        connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(updateMaxProgress(qint64)) );


        player->setPlaylist(player_playlist);

        player->setVolume(50);
        ui->volumeSlider->setValue(50);

        if(!QDir(QDir::homePath() + "/.config/QJukeBox").exists()) {
           QDir().mkdir(QDir::homePath() + "/.config/QJukeBox");
        }

        if(!fileExists(QDir::homePath() + "/.config/QJukeBox/playlist")) {
            QFile file(QDir::homePath() + "/.config/QJukeBox/playlist");
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out(&file);
            out << "";
            file.close();
        }

        listPlaylist();
}

QJukeBox::~QJukeBox()
{
  delete ui;
}

void QJukeBox::updateProgress(qint64 val){
  //qDebug() << "Update Progress Bar:" << val;
  ui->progressBar->setValue(val);
}

void QJukeBox::updateMaxProgress(qint64 val){
  ui->progressBar->setRange(0,val);
}

bool QJukeBox::fileExists(QString path) {
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    return check_file.exists() && check_file.isFile();
}

void QJukeBox::on_file_browser_expanded(const QModelIndex &index) {
    ui->file_browser->resizeColumnToContents(0);
}

void QJukeBox::on_file_browser_collapsed(const QModelIndex &index) {
    ui->file_browser->resizeColumnToContents(0);
}

void QJukeBox::listPlaylist() {
  PlaylistModel->revert();
  playlist.clear();
  QFile playlistfile(QDir::homePath() + "/.config/QJukeBox/playlist");
  playlistfile.open(QIODevice::ReadOnly | QIODevice::Text);
  QTextStream playlistreader(&playlistfile);
  while (true) {
      QString line = playlistreader.readLine();
      if (line.isNull()) {
          break;
      }
      else {
          QFileInfo LineFile(line);
          playlist.append(LineFile.absoluteFilePath());
      }
  }
  PlaylistModel->setStringList(playlist);
  ui->playlist_browser->reset();
  ui->playlist_browser->setModel(PlaylistModel);
  playlistfile.close();
}

void QJukeBox::addToPlaylist() {
  QModelIndex index = ui->file_browser->currentIndex();
  QString FilePath;
  FilePath = FileExplorerModel->filePath(index);
  playlistfile.open(QIODevice::Append | QIODevice::Text);
  QTextStream out(&playlistfile);
  out << FilePath << "\n";
  playlistfile.close();
  listPlaylist();
}

void QJukeBox::directPlay() {
  QModelIndex currentIndex = ui->file_browser->currentIndex();
  QString filename = FileExplorerModel->filePath(currentIndex);
  player->setMedia(QUrl::fromLocalFile(filename));
  player->setVolume(50);
  player->play();
}

void QJukeBox::removeFromPlaylist() {
  QModelIndex index = ui->playlist_browser->currentIndex();
  QString FileName = PlaylistModel->data(index).toString();
  qDebug() << "Filename:" << FileName;

  QStringList PlayList;
  playlistfile.open(QFile::ReadWrite);
  QTextStream playlistStream(&playlistfile);
  while (true) {
      QString line = playlistStream.readLine();
      if (line.isNull()) {
          break;
      }
      else {
          PlayList.append(line);
      }
  }
  playlistfile.close();

  foreach (const QString &f, PlayList) {
         if (f.contains(FileName)) {
              PlayList.removeOne(f);
         }
  }

  playlistfile.open(QFile::WriteOnly|QFile::Truncate);
  playlistStream.flush();
  playlistStream << PlayList.join("\n");
  playlistfile.close();

  listPlaylist();

}

void QJukeBox::on_MainMenu_currentChanged(int index) {
  if(index == 0) {
    listPlaylist();
  }
}

//void QJukeBox::on_play_pause_btn_clicked(bool checked) {
//      QModelIndex currentIndex = ui->playlist_browser->currentIndex();
//      int realIndex = currentIndex.row();
//      QString filename = playlist.value(realIndex);
//      qDebug() << filename;
//      qDebug() << player->state();
//      if(player->currentMedia().isNull()) {
//          player->setMedia(QUrl::fromLocalFile(filename));
//          player->setVolume(50);
//          player->play();
//          ui->play_pause_btn->setIcon(awesome->icon(fa::play));
//        } else {
//          if(player->state() == QMediaPlayer::PlayingState) {
//              player->pause();
//              ui->play_pause_btn->setIcon(awesome->icon(fa::play));
//            } else {
//              player->play();
//              ui->play_pause_btn->setIcon(awesome->icon(fa::pause));
//            }

//      }

      // todo: search in playlist for filename to get absolute path... then play file, add function to determine if file is playling to pause
// }

void QJukeBox::on_playlist_browser_doubleClicked(const QModelIndex &index) {
  QModelIndex currentIndex = ui->playlist_browser->currentIndex();
  int realIndex = currentIndex.row();
  QString filename = playlist.value(realIndex);
  player->setMedia(QUrl::fromLocalFile(filename));
  player->play();
  ui->play_pause_btn->setIcon(awesome->icon(fa::pause));

}

void QJukeBox::on_play_pause_btn_pressed()
{
  QModelIndex currentIndex = ui->playlist_browser->currentIndex();
  int realIndex = currentIndex.row();
  QString filename = playlist.value(realIndex);
  qDebug() << filename;
  qDebug() << player->state();
  if(player->currentMedia().isNull()) {
      player->setMedia(QUrl::fromLocalFile(filename));
      player->play();
      ui->play_pause_btn->setIcon(awesome->icon(fa::pause));
  } else {
      if(player->state() == QMediaPlayer::PlayingState) {
          player->pause();
          ui->play_pause_btn->setIcon(awesome->icon(fa::play));
        } else {

          player->play();
          ui->play_pause_btn->setIcon(awesome->icon(fa::pause));
        }
    }
}

void QJukeBox::on_progressBar_sliderMoved(int position) {
  qDebug() << position;
  player->setPosition(position);
}

void QJukeBox::on_stop_btn_clicked() {
    player->stop();
    ui->play_pause_btn->setIcon(awesome->icon(fa::play));
}

void QJukeBox::on_seek_left_btn_clicked() {
    qint64 cur_pos = player->position();
    qint64 new_pos = cur_pos - 5000;
    qDebug() << "Current Pos: " << cur_pos;
    qDebug() << "New Pos: " << new_pos;
    player->pause();
    player->setPosition(new_pos);
    player->play();
    ui->progressBar->setValue(new_pos);
}

void QJukeBox::on_seek_right_btn_clicked() {
    qint64 cur_pos = player->position();
    qint64 new_pos = cur_pos + 5000;
    qDebug() << "Current Pos: " << cur_pos;
    qDebug() << "New Pos: " << new_pos;
    player->pause();
    player->setPosition(new_pos);
    player->play();
    ui->progressBar->setValue(new_pos);
}

void QJukeBox::on_exit_btn_clicked() {
    qApp->exit();
}

void QJukeBox::on_volumeSlider_valueChanged(int value) {
    player->setVolume(value);
}

void QJukeBox::playItem() {
  QModelIndex currentIndex = ui->playlist_browser->currentIndex();
  int realIndex = currentIndex.row();
  QString filename = playlist.value(realIndex);
  player->setMedia(QUrl::fromLocalFile(filename));
  player->play();
  ui->play_pause_btn->setIcon(awesome->icon(fa::pause));
}
